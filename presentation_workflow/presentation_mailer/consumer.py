import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        # CREATE A FUNCTION THAT WILL PROCESS THE MESSAGE WHEN IT ARRIVES
        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)

            subject = "Your presentation has been accepted"
            message = "{presenter_name}, we are happy to tell you that your presentation {title} has been accepted"
            from_email = 'admin@conference.go'
            recipient_list = ["{presenter_email}"]

            send_mail(
                subject,
                message,
                from_email,
                recipient_list,
                fail_silently=False,
            )


        def process_rejection(ch, method, properties, body):
            print("  Received %r" % body)

            subject = "Your presentation has been rejected"
            message = "{presenter_name}, we regret to inform you that your presentation {title} has been rejected"
            from_email = 'admin@conference.go'
            recipient_list = ["{presenter_email}"]

            send_mail(
                subject,
                message,
                from_email,
                recipient_list,
                fail_silently=False,
            )
        # SET THE HOSTNAME THAT WE'LL CONNECT TO
        parameters = pika.ConnectionParameters(host='rabbitmq')

        # CREATE A CONNECTION TO RABBITMQ
        connection = pika.BlockingConnection(parameters)

        # OPEN A CHANNEL TO RABBITMQ
        channel = connection.channel()

        # CREATE A QUEUE IF IT DOESN'T EXIST
        channel.queue_declare(queue='presentation_approvals')

        # CONFIGURE THE CONSUMER TO CALL THE PROCESS_APPROVAL FUNCTION
        # WHEN A MESSAGE ARRIVES
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )

        # CREATE A QUEUE IF IT DOESN'T EXIST
        channel.queue_declare(queue='presentation_rejections')

        # CONFIGURE THE CONSUMER TO CALL THE PROCESS_APPROVAL FUNCTION
        # WHEN A MESSAGE ARRIVES
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        # TELL RABBITMQ THAT YOU'RE READY TO RECEIVE MESSAGES
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
